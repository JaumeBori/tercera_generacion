# Generacions dels llenguatges de programació
___
[Primera_Generació](https://gitlab.com/53335394g/primera_generacion/blob/master/primera_generaci%C3%B3n.md)

[Segona_Generació](https://gitlab.com/JaumeBori/generacion-2-/blob/master/SegonaGeneracio.md)

[Tercera-Generació](https://gitlab.com/JaviEV/tercera_generacion/blob/master/tercera_generacion.md)

[Quarta_Generació](https://gitlab.com/53335394g/primera_generacion/blob/master/cuarta_generaci%C3%B3n.md)

[Cinquena-Generació](https://gitlab.com/JaumeBori/generacion-2-/blob/master/5gen.md)

[Preguntes](https://gitlab.com/JaumeBori/generacion-2-/blob/master/fi.md)
